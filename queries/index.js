module.exports = {
  GET_ALL_PRODUCTS: `SELECT
    docs.date as createdAt,
    docTypes.name as docTypesName,
    docTypes.id as docTypesId,
    products.image as productImage,
    products.name as productName,
    products.price as productPrice,
    docs.id as docId,
    rows.quantity as productQuantity
  FROM rows
    LEFT JOIN docs
        ON docs.id = rows.docId
    JOIN docTypes
        ON docTypes.id = docs.typeId
    JOIN products
        on products.id = rows.productId
  WHERE docs.removed = false AND products.id = rows.productId
  ORDER BY docs.id
  `
}
