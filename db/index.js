const sqlite3 = require('sqlite3').verbose()
const path = require('path')
const dbOptions = sqlite3.OPEN_READONLY
const dbName = path.join(__dirname, `./${process.env.DB_NAME}`)

const db = new sqlite3.Database(dbName, dbOptions, (err) => {
    if (err) console.error(err.message)
    console.log('Connected to the sysTech database.')
  }
)

const getDb = () => db

const closeDb = () => db.close()

module.exports = {
  db,
  getDb,
  closeDb
}
