const express = require('express');
const {port} = require('./config')
const path = require('path')

const app = express();
app.disable('x-powered-by')


// views
app.set('view engine', 'ejs');

app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({ extended: false }));

app.get('/', require('./routes/api/home'))

app.listen(port, () => {
    console.log(`listening at http://localhost:${port}`)
})
