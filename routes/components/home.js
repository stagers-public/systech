const { getDb, closeDb } = require('../../db')
const { GET_ALL_PRODUCTS } = require('../../queries')

const homePage = (req, res, next) => {
  
  return getDb().all(GET_ALL_PRODUCTS, (err, rows) => {
    
    if (err) {
      return console.error(err.message)
    }
    
    let sum = null
    const preparedRows = rows.reduce((out, arg) => {
      const outDate = arg.createdAt.split(' ')[0]
      if (!(out[outDate])) {
        out[outDate] = {
          data: {},
          totalPrice: null,
          length: null
        }
      }
      
      const totalSum = parseFloat(arg.productPrice) * arg.productQuantity
      
      out[outDate].totalPrice += totalSum
      out[outDate].length += 1
      
      if (!out[outDate].data[arg.docId]) {
        sum = null
        out[outDate].data[arg.docId] = {
          items: [],
          sum: null,
          docTypesName: arg.docTypesName
        }
      }
      
      sum = parseFloat(arg.productPrice) * arg.productQuantity
      out[outDate].data[arg.docId].items.push(arg)
      out[outDate].data[arg.docId].sum += sum
      
      return out
    }, {})
    
    // res.json({ preparedRows })
    
    res.render('index', {
      title: 'Products list',
      lists: preparedRows
    })
    
    // closeDb();
  })
  
}

module.exports = {
  homePage
}
