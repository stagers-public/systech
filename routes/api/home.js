const express = require('express')
const router = express.Router()
const {
    homePage
} = require('../components/home')

// @route   GET api/homePage/
// @desc    Главная страница
// @access  Public
router.get(`/`, homePage)

module.exports = router
