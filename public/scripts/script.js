const sectionHeader = document.querySelectorAll('.sectionHeader')

const elementClick = (elem) => {
  return elem
    .addEventListener(
      'click',
      () => {
        elem.parentElement.classList.toggle('show')
      }
    )
}

sectionHeader.forEach(item => {
  
  elementClick(item)
  
  const listBtns = item.querySelectorAll('.itemGoodsList')
  
  listBtns.forEach(btn => {
    
    const goods = btn.nextSibling
    
    btn.addEventListener('click', () => {
      
      goods.classList.toggle('show')
      
    })
  })
  
})
