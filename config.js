const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    port: process.env.PORT || 4000,
    DB_NAME: process.env.DB_NAME
}

